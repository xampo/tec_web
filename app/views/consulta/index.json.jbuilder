json.array!(@consulta) do |consultum|
  json.extract! consultum, :id, :descripcion
  json.url consultum_url(consultum, format: :json)
end
