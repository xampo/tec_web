json.array!(@comentarios) do |comentario|
  json.extract! comentario, :id, :descripcion, :fecha
  json.url comentario_url(comentario, format: :json)
end
