json.array!(@archivos) do |archivo|
  json.extract! archivo, :id, :url, :descripcion, :puntuacion
  json.url archivo_url(archivo, format: :json)
end
