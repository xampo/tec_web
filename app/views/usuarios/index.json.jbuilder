json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :email, :password, :isAdmin
  json.url usuario_url(usuario, format: :json)
end
