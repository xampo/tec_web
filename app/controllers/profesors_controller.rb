class ProfesorsController < ApplicationController
  before_action :set_profesor, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:create]
  # GET /profesors
  # GET /profesors.json
  def index
    if params[:curso]
      @curso = Curso.where(id: params[:curso]).first
      @profesors = @curso.profesors.paginate(:per_page => 5, :page => params[:page])

    else
      @profesors = Profesor.all.paginate(:per_page => 5, :page => params[:page])

    end
  end

  # GET /profesors/1
  # GET /profesors/1.json
  def show
  end

  # GET /profesors/new
  def new
    @profesor = Profesor.new
  end

  # GET /profesors/1/edit
  def edit
  end

  # POST /profesors
  # POST /profesors.json
  def create
    @profesor = Profesor.new
    @profesor.nombre = profesor_params[:nombre]
    @profesor.descripcion = profesor_params[:descripcion]
    
    if profesor_params[:curso_id] != ""

          Curso.find(profesor_params[:curso_id]).profesors << @profesor
          respond_to do |format|
            if @profesor.save
              format.html { redirect_to profesor_path(@profesor,:curso =>Curso.find(profesor_params[:curso_id]).id), notice: 'Profesor fue creado con exito.' }
              format.json { render :show, status: :created, location: @profesor }
            else
              format.html { render :new }
              format.json { render json: @profesor.errors, status: :unprocessable_entity }
            end
          end
    else
            respond_to do |format|
            if @profesor.save
              format.html { redirect_to @profesor, notice: 'Profesor was successfully created.' }
              format.json { render :show, status: :created, location: @profesor }
            else
              format.html { render :new }
              format.json { render json: @profesor.errors, status: :unprocessable_entity }
            end
          end
    end

  end

  # PATCH/PUT /profesors/1
  # PATCH/PUT /profesors/1.json
  def update
    respond_to do |format|
       @profesor.nombre = profesor_params[:nombre]
       @profesor.descripcion = profesor_params[:descripcion]

      if @profesor.save
        format.html { redirect_to profesor_path(@profesor,:curso => profesor_params[:curso_id]), notice: 'Profesor was successfully updated.' }
        format.json { render :show, status: :ok, location: @profesor }
      else
        format.html { render :edit }
        format.json { render json: @profesor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profesors/1
  # DELETE /profesors/1.json
  def destroy
    @profesor.destroy
     if params[:curso]
      respond_to do |format|
        format.html { redirect_to profesors_path(:curso => params[:curso]), notice: 'Profesor was successfully destroyed.' }
       format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to profesors_url, notice: 'Profesor was successfully destroyed.' }
       format.json { head :no_content }
     end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profesor
      @profesor = Profesor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profesor_params
      params.require(:profesor).permit(:nombre, :descripcion, :curso_id)
    end
end
