class ConsultaController < ApplicationController
  before_action :set_consultum, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:create]
  # GET /consulta
  # GET /consulta.json
  
  def index
    
    if params[:curso]
      @curso = Curso.where(id: params[:curso]).first
      @consulta = @curso.consulta.search(params[:search]).paginate(:per_page => 5, :page => params[:page])
    elsif params[:consultum_id]
      @consultum = Consultum.where(id: params[:consultum_id]).first
      @consulta = @consultum.consulta.search(params[:search]).paginate(:per_page => 5, :page => params[:page])
    else
      @consulta = Consultum.all.search(params[:search]).paginate(:per_page => 5, :page => params[:page])
    end

  end


  # GET /consulta/1
  # GET /consulta/1.json
  def show
  end

  # GET /consulta/new
  def new
    @consultum = Consultum.new
  end

  # GET /consulta/1/edit
  def edit
  end

  # POST /consulta
  # POST /consulta.json
  def create
    @consultum = Consultum.new(consultum_params)
    ######################cOnsulta  creacion
      
        if consultum_params[:consultum_id] != ""
        Consultum.find(consultum_params[:consultum_id]).related_consulta << @consultum
        respond_to do |format|
          if @consultum.save

            format.html { redirect_to consultum_path(@consultum, :curso => consultum_params[:curso_id]), notice: 'Consultum was successfully created.' }
            format.json { render :show, status: :created, location: @consultum }
          else
            format.html { render :new }
            format.json { render json: @consultum.errors, status: :unprocessable_entity }
          end
        end
      else
        respond_to do |format|
          if @consultum.save
            format.html { redirect_to consultum_path(@consultum, :curso => consultum_params[:curso_id]), notice: 'Consultum was successfully created.' }
            format.json { render :show, status: :created, location: @consultum }
          else
            format.html { render :new }
            format.json { render json: @consultum.errors, status: :unprocessable_entity }
          end
        end

      end
  
    
    
   
  end

  # PATCH/PUT /consulta/1
  # PATCH/PUT /consulta/1.json
  def update
    respond_to do |format|
      if @consultum.update(consultum_params)
        format.html { redirect_to @consultum, notice: 'Consultum was successfully updated.' }
        format.json { render :show, status: :ok, location: @consultum }
      else
        format.html { render :edit }
        format.json { render json: @consultum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consulta/1
  # DELETE /consulta/1.json
  def destroy
    @consultum.destroy

    respond_to do |format|
      if params[:curso]
        format.html { redirect_to consulta_path( :curso => params[:curso]), notice: 'Consultum was successfully updated.' }
        format.json { render :show, status: :ok, location: @consultum }
      else
        format.html { redirect_to consulta_url, notice: 'Consultum was successfully destroyed.' }
        format.json { head :no_content }
      end
     
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consultum
      @consultum = Consultum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def consultum_params
      params.require(:consultum).permit(:descripcion, :curso_id, :consultum_id)
    end
end
