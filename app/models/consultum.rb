class Consultum < ActiveRecord::Base
	belongs_to :curso
	
	has_many :comentarios
	
	

	 has_many :related_consulta_association, :class_name => "RelatedConsultum"
  has_many :related_consulta, :through => :related_consulta_association, :source => :related_consultum
  has_many :inverse_related_consulta_association, :class_name => "RelatedConsultum", :foreign_key => "related_consultum_id"
  has_many :inverse_related_consulta, :through => :inverse_related_consulta_association, :source => :consultum
  def self.search(search)
	    if search
	      where('descripcion LIKE ?', "%#{search}%")
	    else
	      all
	    end
  end
end
