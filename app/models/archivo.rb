class Archivo < ActiveRecord::Base
	acts_as_voteable
	belongs_to :Curso
	def self.search(search)
	    if search
	      where('descripcion LIKE ?', "%#{search}%")
	    else
	      all
	    end
  end

end
