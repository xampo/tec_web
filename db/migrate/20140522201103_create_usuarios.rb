class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :correo,              null: false, default: ""
      t.string :contrasena,              null: false, default: ""

      t.timestamps
    end
  end
end
