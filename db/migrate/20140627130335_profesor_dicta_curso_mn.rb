class ProfesorDictaCursoMn < ActiveRecord::Migration
  def self.up
    create_table :profesor_dicta_cursos, :id => false do |t|
      t.integer :curso_id
      t.integer :profesor_id
    end

    add_index :profesor_dicta_cursos, [:curso_id, :profesor_id]
  end

  def self.down
    drop_table :profesor_dicta_cursos
  end
end
