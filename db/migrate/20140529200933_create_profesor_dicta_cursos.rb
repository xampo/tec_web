class CreateProfesorDictaCursos < ActiveRecord::Migration
  def change
    create_table :profesor_dicta_cursos do |t|
      t.integer :profesorId
      t.integer :cursoId
      t.text :semestre

      t.timestamps
    end
  end
end
