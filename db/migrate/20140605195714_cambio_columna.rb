class CambioColumna < ActiveRecord::Migration
  def change
  	rename_column :Usuarios, :correo, :email 
  	rename_column :Usuarios, :contrasena, :password
  end
end
