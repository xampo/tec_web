class CambioColumnas < ActiveRecord::Migration
  def change
  	rename_column :Archivos, :cursoId, :curso_id 
  	rename_column :Profesor_dicta_cursos, :cursoId, :curso_id
  	rename_column :Profesor_dicta_cursos, :profesorId, :profesor_id
  end
end
