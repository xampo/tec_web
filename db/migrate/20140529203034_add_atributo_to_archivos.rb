class AddAtributoToArchivos < ActiveRecord::Migration
  def change
    add_column :archivos, :cantVotos, :integer
    add_column :archivos, :cursoId, :integer
  end
end
