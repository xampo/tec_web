class CreateRelatedConsulta < ActiveRecord::Migration
  def change
    create_table :related_consulta do |t|
    	t.integer  :consultum_id
    	t.integer  :related_consultum_id
		t.timestamps
    end
  end
end
