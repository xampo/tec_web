class CreateArchivos < ActiveRecord::Migration
  def change
    create_table :archivos do |t|
      t.string :url
      t.text :descripcion
      t.integer :puntuacion

      t.timestamps
    end
  end
end
