# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140717205739) do

  create_table "Archivos", force: true do |t|
    t.string   "url"
    t.text     "descripcion"
    t.integer  "puntuacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cantVotos"
    t.integer  "curso_id"
  end

  create_table "Usuarios", force: true do |t|
    t.string   "email"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "isAdmin"
  end

  add_index "Usuarios", ["email"], name: "index_usuarios_on_email", unique: true
  add_index "Usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true

  create_table "comentarios", force: true do |t|
    t.text     "descripcion"
    t.date     "fecha"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "usuario_id"
  end

  create_table "consulta", force: true do |t|
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "curso_id"
    t.integer  "consultum_id"
  end

  create_table "consultum", force: true do |t|
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cursos", force: true do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cursos_profesors", force: true do |t|
    t.integer  "profesor_id"
    t.integer  "curso_id"
    t.text     "semestre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profesor_dicta_cursos", id: false, force: true do |t|
    t.integer "curso_id"
    t.integer "profesor_id"
  end

  add_index "profesor_dicta_cursos", ["curso_id", "profesor_id"], name: "index_profesor_dicta_cursos_on_curso_id_and_profesor_id"

  create_table "profesors", force: true do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "related_consulta", force: true do |t|
    t.integer  "consultum_id"
    t.integer  "related_consultum_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "votes", force: true do |t|
    t.boolean  "vote",          default: false, null: false
    t.integer  "voteable_id",                   null: false
    t.string   "voteable_type",                 null: false
    t.integer  "voter_id"
    t.string   "voter_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["voteable_id", "voteable_type"], name: "index_votes_on_voteable_id_and_voteable_type"
  add_index "votes", ["voter_id", "voter_type", "voteable_id", "voteable_type"], name: "fk_one_vote_per_user_per_entity", unique: true
  add_index "votes", ["voter_id", "voter_type"], name: "index_votes_on_voter_id_and_voter_type"

end
